package com.nccgroup.drinksuggest;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class DrinkListActivity extends Activity {

	protected List<ParseObject> mMessages;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drink_list);
		
		 // Get the reference of ListViewAnimals
        final ListView drinkList=(ListView)findViewById(R.id.listViewDrinks);
		
		setProgressBarIndeterminateVisibility(true);
		ParseQuery<ParseObject> query = new ParseQuery("DrinkSuggest");
		query.whereEqualTo("sender_id", ParseUser.getCurrentUser().getObjectId());
		query.addDescendingOrder("createdAt");
		query.findInBackground(new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> messages, ParseException e) {
				// TODO Auto-generated method stub
				setProgressBarIndeterminateVisibility(false);
				
				if(e == null){
					mMessages = messages;
					String[] drinks = new String[mMessages.size()];
					int i = 0;
					for(ParseObject message : mMessages){
						drinks[i] = message.getString("name");
						i++;
					}
					
					ArrayAdapter<String> arrayAdapter =      
			                 new ArrayAdapter<String>(DrinkListActivity.this,android.R.layout.simple_list_item_1, drinks);
					drinkList.setAdapter(arrayAdapter);
					
				}
				
				
			}});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.drink_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

package com.nccgroup.drinksuggest;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class.getSimpleName();
	public static final int TAKE_PHOTO_REQUEST = 0;
	public static final int PICK_PHOTO_REQUEST = 1;
	public static final int MEDIA_TYPE_IMAGE = 2;
	public static final int MEDIA_TYPE_VIDEO = 3;
	Uri mMediaUri;
	ImageView img ;
	Button suggestBtn;
	EditText message, drinkName;
	
	DialogInterface.OnClickListener mDialogListener = new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			switch(which){
				case 0: // take picture
					Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					takePhotoIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					takePhotoIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					mMediaUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
					img.setImageURI(mMediaUri);
					if(mMediaUri == null){
						//display error
						Toast.makeText(MainActivity.this, R.string.error_external_storage, Toast.LENGTH_LONG).show();
					}else{
						takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, mMediaUri);
						startActivityForResult(takePhotoIntent, TAKE_PHOTO_REQUEST);
					}
					
					break;
				case 1:// select picture from gallery
					Intent choosePhotoIntent = new Intent(Intent.ACTION_GET_CONTENT);
					choosePhotoIntent.setType("image/*");
					startActivityForResult(choosePhotoIntent,PICK_PHOTO_REQUEST);
					break;
			}
		}
	};
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser == null) {
			goToLogin();
		}
		else {
			Log.i(TAG, currentUser.getUsername());
		}
		
		img = (ImageView)findViewById(R.id.imageViewDrink);
		
		suggestBtn = (Button)findViewById(R.id.buttonSuggest);
		suggestBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 message = (EditText) findViewById(R.id.editTextMessage);
				 String messageTxt = message.getText().toString().trim();
				 
				drinkName = (EditText) findViewById(R.id.editTextDrinkName);
				 String drinkNameTxt = drinkName.getText().toString().trim();
				 
				 ParseObject message = createMessage(messageTxt,drinkNameTxt);
				 send(message);
				 
				
			}
		});
		
    }
    
    protected void send(ParseObject message){
    	message.saveInBackground(new SaveCallback(){

			@Override
			public void done(ParseException e) {
				// TODO Auto-generated method stub
				if(e == null){
					Toast.makeText(MainActivity.this, "Suggestion sent!", Toast.LENGTH_LONG).show();
				}
			}});
    	
    }
    
    protected ParseObject createMessage(String msg, String drinkName){
    	ParseObject obj = new ParseObject("DrinkSuggest");
    	obj.put("sender_id",ParseUser.getCurrentUser().getObjectId());
    	obj.put("referral",ParseUser.getCurrentUser().getUsername());
    	obj.put("referralReason",msg);
    	obj.put("approvalStatus", "pending");
    	obj.put("approved", false);
    	obj.put("decisionReason", "");
    	obj.put("name", drinkName);
    	
    	byte[] fileBytes = FileHelper.getByteArrayFromFile(this, mMediaUri);
    	if (fileBytes == null){
    		return null;
    	}else{
    		fileBytes = FileHelper.reduceImageForUpload(fileBytes);
    		String fileName = FileHelper.getFileName(this, mMediaUri, "image");
    		ParseFile file = new ParseFile(fileName,fileBytes);
    		obj.put("drinkImage",file);
    	}
    	
    
    	return obj;
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(requestCode, resultCode, data);
    	
		if (resultCode == RESULT_OK) {

			if (requestCode == PICK_PHOTO_REQUEST) {
				if (data == null) {
					// error to handle
				} else {
					mMediaUri = data.getData();
					img.setImageURI(mMediaUri);
				}
			} else {
				// add to Gallery
				Intent mediaScanIntent = new Intent(
						Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				mediaScanIntent.setData(mMediaUri);
				img.setImageURI(mMediaUri);
				sendBroadcast(mediaScanIntent);
			}
		} else if (resultCode != RESULT_CANCELED) {
			Toast.makeText(this, "Sorry error", Toast.LENGTH_LONG).show();
		}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id){
        case R.id.action_logout:
        	ParseUser.logOut();
        	goToLogin();
        case R.id.action_camera:        	
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	builder.setItems(R.array.camera_choices, mDialogListener);
        	AlertDialog dialog = builder.create();
        	dialog.show();
        case R.id.action_my_drinks:
        	goToMyDrinks();
        	
        }
        return super.onOptionsItemSelected(item);
    }
    
	private void goToLogin() {
		Intent intent = new Intent(this,LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}
	
	private void goToMyDrinks(){
		Intent intent = new Intent(this,DrinkListActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}
	
	private  Uri getOutputMediaFileUri(int mediaType){
		
		if(isExternalStorageAvailable()){
			
			//1.get external storage
			String appName = MainActivity.this.getString(R.string.app_name);
			File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),appName);
			
			//2.create file name
			if( !mediaStorageDir.exists()){
				if(!mediaStorageDir.mkdirs()){
					Log.e(TAG, "Failed to create directory");
					return null;
				}
			}
			//3.create file
			
			//4.return uri
			File mediaFile = null;
			Date now = new Date();
			String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.ENGLISH).format(now);
			String path = mediaStorageDir.getPath() + File.separator;
			
			if(mediaType == MEDIA_TYPE_IMAGE){
				mediaFile = new File(path + "IMG_" + timestamp + ".jpg");
			}
			Log.d(TAG,"File: "+Uri.fromFile(mediaFile));
			//5.Return URI
			return Uri.fromFile(mediaFile);
		}else{
			return null;
		}
			
	} 
	
	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.

	    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	              Environment.DIRECTORY_PICTURES), "MyCameraApp");
	    // This location works best if you want the created images to be shared
	    // between applications and persist after your app has been uninstalled.

	    // Create the storage directory if it does not exist
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("MyCameraApp", "failed to create directory");
	            return null;
	        }
	    }

	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "IMG_"+ timeStamp + ".jpg");
	    } else if(type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "VID_"+ timeStamp + ".mp4");
	    } else {
	        return null;
	    }

	    return mediaFile;
	}
	
	private boolean isExternalStorageAvailable(){
		String state = Environment.getExternalStorageState();
		if(state.equals(Environment.MEDIA_MOUNTED)){
			return true;
		}else{
			return false;
		}
	}
}

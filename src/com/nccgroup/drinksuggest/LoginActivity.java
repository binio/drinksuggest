package com.nccgroup.drinksuggest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends Activity {

	EditText mUsername;
	EditText mPassword;
	Button   mLoginBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		mUsername = (EditText) findViewById(R.id.editTextUsername);
		mPassword = (EditText) findViewById(R.id.editTextPasswordLogin);
		mLoginBtn = (Button) findViewById(R.id.buttonLogin);
		
		TextView tv = (TextView) findViewById(R.id.textViewRegister);
		registerUser(tv);
		
		mLoginBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String username = mUsername.getText().toString().trim();
				String password = mPassword.getText().toString().trim();
				
				if(	username.isEmpty() ||  password.isEmpty()){
					AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
					builder.setMessage(R.string.signup_error_message)
					.setTitle(R.string.signup_error_title)
					.setPositiveButton(android.R.string.ok, null);
					AlertDialog dialog = builder.create();
					dialog.show();
				} 
				 else {

					ParseUser.logInInBackground(username, password,
							new LogInCallback() {
								public void done(ParseUser user,
										ParseException e) {
									if (user != null) {
										// Hooray! The user is logged in.
										Intent intent = new Intent(
												LoginActivity.this,
												MainActivity.class);
										intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
										startActivity(intent);
									} else {
										// Signup failed. Look at the
										// ParseException to see what happened.
										AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
										builder.setMessage(e.getMessage())
										.setTitle(R.string.signup_error_title)
										.setPositiveButton(android.R.string.ok, null);
										AlertDialog dialog = builder.create();
										dialog.show();
									}
								}
							});

				}
			}
		});
		
		
	}

	private void registerUser(TextView tv) {
		tv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {        	
            	goToRegistration();
             }

			private void goToRegistration() {
				Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
        		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        		startActivity(intent);
			}
            });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
